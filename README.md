# Project UniqueDesign

Sajt na kome će ulogovani korisnici moći sami da dizajniraju neke od artikala(odevni predmeti) i potom ih naručuju.
- registracija novih korisnika
- log in korisnika
- korisnici sami biraju i dizajniraju artikal
- dizajniranje artikla obuhvata: biranje boje artikla; kreiranje teksta, biranje fonta, lepljenje slike, pozicioniranje dodataka na artiklu
- korisnici biraju neki od postojećih modela i naručuju ga
- korisnici biraju neki od postojećih modela uz mogućnost edotovanja
- kupovina kreiranog modela

## Developers

- [Maja Vukolić, 103/2016](https://gitlab.com/maja97)
- [Milica Marić, 153/2016](https://gitlab.com/m_milica)
- [Radiša Mitrović, 429/2019](https://gitlab.com/mowgliluciano)
- [Maja Gavrilović, 489/2017](https://gitlab.com/majakovska)
- [Andrijana Čakarević, 494/2018](https://gitlab.com/andrijanica)

## Tehnologije
- Angular (client)
- Node.js/Express.js (server)
- MongoDB

## [Izvestaji](https://gitlab.com/matfpveb/projekti/2020-2021/17-UniqueDesign/-/wikis/home)

## [Build procedura](https://gitlab.com/matfpveb/projekti/2020-2021/17-UniqueDesign/-/wikis/Build-procedura)

## [Opis baze podataka](https://gitlab.com/matfpveb/projekti/2020-2021/17-UniqueDesign/-/wikis/Opis-baze-podataka)

## [Demo snimak](https://youtu.be/x1SyDP3YL0w) 
