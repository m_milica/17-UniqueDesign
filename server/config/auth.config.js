const jwt_secret = process.env.JWT_SECRET || "secret";
const SALT_ROUNDS = 10;

module.exports = {
    jwt_secret,
    SALT_ROUNDS
};