const mongoose = require('mongoose');
const imageToolsModel = require('../models/imageTools');


const getAllImageTools = async (req, res, next) => {
	try {
		const allImageTools = await imageToolsModel.find({productId: req.query.productId}).exec();
		res.status(200).json(allImageTools);
	} catch (error) {
	 	next(error);
	}
};

const getImageToolById = async (req, res, next) => {
	try {
		const imageTool = await imageToolsModel.findById(req.params.id).exec();
		if(!imageTool) {
			res.status(404).json("Ne postoji imageTool sa zadatim id-em");		
		}
		else{
			res.status(200).json(imageTool);
		}
	} catch (error) {
		next(error);
	}
};


const addNewImageTool = async (req, res, next) => {
	const {imageContent, opacity, width, height, positionX, positionY, productId, flag} = req.body;

	if((!imageContent) || (!opacity) || (!width) || (!height) || (!positionX) || (!positionY) || (!productId) || (!flag)) {
		res.status(400).json("Greska u zahtevu - nedostaje neki parametar");
	} else {
		try {
			const newImageTool = new imageToolsModel({
					_id: new mongoose.Types.ObjectId(),
					imageContent: imageContent,
					opacity: opacity,
					width: width,
					height: height,
					positionX: positionX,
					positionY: positionY,
					productId: productId,
					flag: flag
			});
			await newImageTool.save();
			res.status(201).json(newImageTool);
		} catch (error) {
			next(error);
		}
	}
};

const changeImageTool = async(rew, res, next) => {
	const {newImageContent, newOpacity, newWidth, newHeight, newPositionX, newPositionY, newProductId, newFlag} = req.body;
	
	if((!newImageContent) || (!newOpacity) || (!newWidth) || (!newHeight) || (!newPositionX) || (!newPositionY) || (!newProductId) || (!newFlag)) {
		res.status(400).json("Greska u zahtevu - nedostaje neki parametar");
	} else {
		try {
			const imageTool = await imageToolsModel.findById(req.params.id).exec();
			if(!imageTool) {
				res.status(404).json("Ne postoji imageTool sa zadatim id-em");
			}
			else {
				imageTool.imageContent = newImageContent;
				imageTool.opacity = newOpacity;
				imageTool.width = newWidth;
				imageTool.height = newHeight;
				imageTool.positionX = newPositionX;
				imageTool.positionY = newPositionY;
				imageTool.productId = newProductId;
				imageTool.flag = newFlag;

				await imageTool.save();
				res.status(200).json("Slika je uspesno azurirana");
			}
		} catch (error) {
			next(error);
		}
	}
};

const deleteImageTool = async(req, res, next) => {
	try {
		const imageTool = await imageToolsModel.findById(req.params.id).exec();
		if(!imageTool) {
			res.status(404).json("Neuspelo brisanje slike");
		} else {
			await imageToolsModel.findByIdAndDelete(req.params.id).exec();
			res.status(200).json("Slika je uspesno obrisana");
		}
	} catch(error) {
		next(error);	
	}
};


module.exports = {
	getAllImageTools,
	getImageToolById,
	addNewImageTool,
	changeImageTool,
	deleteImageTool
};
