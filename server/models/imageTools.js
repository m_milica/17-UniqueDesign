const mongoose = require('mongoose');
const uuid = require('uuid');


const imageToolsSchema = new mongoose.Schema ({
	_id: mongoose.Schema.Types.ObjectId,
	imageContent: {
		type: String,
		required: true
	},
	opacity: {
		type: Number,
		default: 100
	},
	width: {
		type: Number,
		required: true
	},
	height: {
		type: Number,
		required: true
	},
	positionX: {
		type: Number,
		required: true
	},
	positionY: {
		type: Number,
		required: true
	},
	productId: {
		type: mongoose.Schema.Types.ObjectId,
        ref: 'products',
        required: true
	},
	flag: {
		type: String,
		enum: ['f', 'b'],
		required: true
	}
});

const imageToolsModel = mongoose.model('imageTools', imageToolsSchema);

module.exports = imageToolsModel;
