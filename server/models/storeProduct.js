const mongoose = require('mongoose');

const storeProductSchema = mongoose.Schema({
  _id: mongoose.Schema.Types.ObjectId,
  products: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'products',
    required: true,
  }],
  name: {
    type: String,
    required: true,
  },
  address: {
    type: String,
    required: true,
  },
  email: {
    type: String,
    required: true,
  },
});

module.exports = mongoose.model('storeproduct', storeProductSchema);