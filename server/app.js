const path = require('path');
const fs = require('fs');
global.__uploadDir = path.join(__dirname, 'resources', 'uploads');

console.log(__uploadDir);
if (!fs.existsSync(__uploadDir)) {
  fs.mkdirSync(__uploadDir);
}

const express = require('express');
const {json} = require('body-parser');
const mongoose = require('mongoose');


const app = express();

const databaseString = 'mongodb://localhost:27017/uniqueDesign';
mongoose.connect(databaseString, {
  useNewUrlParser: true,
  useUnifiedTopology: true
});
mongoose.connection.once('open', function(){
  console.log('Uspesno povezivanje!');
});
mongoose.connection.on('error', (error) =>{
  console.log('Greska:', error);
});

const userAPI = require('./routes/user');
const clothingItemsAPI = require('./routes/clothingItems');
const productAPI = require('./routes/products');
const designAPI = require('./routes/design');

//tekst koji se nalazi na proizvodu
const textToolAPI = require('./routes/textTools');
const imageToolAPI = require('./routes/imageTools');
const storeProductAPI = require('./routes/storeProduct');

app.use(json({limit: '50mb'}));

//CORS zastita
app.use(function (req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization');

  if (req.method === 'OPTIONS') {
    res.header('Access-Control-Allow-Methods', 'OPTIONS, GET, POST, PATCH, PUT, DELETE');

    return res.status(200).json({});
  }

  next();
});


app.use("/users/", userAPI);

app.use("/clothingItems/", clothingItemsAPI);

app.use("/products/", productAPI);

app.use("/design/", designAPI);

app.use("/textTools/", textToolAPI);

app.use("/imageTools/", imageToolAPI);

app.use("/storeProducts/", storeProductAPI);

app.use(express.static(__uploadDir));

app.use(function (req, res, next) {
    const error = new Error('Zahtev nije podrzan!');
    error.status = 405;
    next(error);
});
  
app.use(function (error, req, res, next) {
    res.status(error.status || 500).json(
      { message: error.message});
    });
  
module.exports = app;
