const express = require('express');
const router = express.Router();
const jwtAuth = require('../utils/authentication');
const productController = require('../controllers/products');

router.get("/", productController.getAllPublicProducts);
router.get("/user",jwtAuth.verifyToken, productController.getProductsByUsername);
router.get("/:id", productController.getProductById);

router.post("/", jwtAuth.verifyToken, productController.addNewProduct);

router.put("/:id", productController.changeProduct);

router.delete("/:id", productController.deleteProduct);


router.use((req, res, next) => {
    const unknownMethod = req.method;
    const unknownPath = req.path;
    res.status(500).json(`mesage: Nepoznat zahtev ${unknownMethod} ka ${unknownPath}!`);
});

module.exports = router;