const express = require('express');
const imageToolsController = require('../controllers/imageTools');

const router = express.Router();

router.get("/", imageToolsController.getAllImageTools);

router.get("/:id", imageToolsController.getImageToolById);

router.post("/", imageToolsController.addNewImageTool);

router.put("/:id", imageToolsController.changeImageTool);

router.delete("/:id", imageToolsController.deleteImageTool);

router.use((req, res, next) => {
	const unknownMethod = req.method;
	const unknownPath = req.path;
	res.status(500).json(`message: Nepoznat zahtev ${unknownMethod} ka ${unknownPath}!`);
});

module.exports = router;

