const express = require('express');
const router = express.Router();
const jwtAuth = require('../utils/authentication');
const clothingItemController = require('../controllers/clothingItems');

router.get("/", clothingItemController.getAllClothingItems);
router.get("/:id", clothingItemController.getClothingItemById);

router.post("/", clothingItemController.addNewClothingItem);

router.delete("/:id", clothingItemController.deleteClothingItem);

router.use((req, res, next) => {
    const unknownMethod = req.method;
    const unknownPath = req.path;
    res.status(500).json(`mesage: Nepoznat zahtev ${unknownMethod} ka ${unknownPath}!`);
});

module.exports = router;