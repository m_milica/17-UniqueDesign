const express = require('express');

const router = express.Router();

const controller = require('../controllers/storeProduct');

router.get('/', controller.getStoreProducts);
router.post('/', controller.createAnStoreProduct);

router.get('/:id', controller.getAnStoreProductById);

module.exports = router;
