const express = require('express');
const textToolsController = require('../controllers/textTools');

const router = express.Router();

router.get("/", textToolsController.getAllTextTools);

router.get("/:id", textToolsController.getTextToolById);

router.post("/", textToolsController.addNewTextTool);

router.put("/:id", textToolsController.changeTextTool);

router.delete("/:id", textToolsController.deleteTextTool);

router.use((req, res, next) => {
    const unknownMethod = req.method;
    const unknownPath = req.path;
    res.status(500).json(`mesage: Nepoznat zahtev ${unknownMethod} ka ${unknownPath}!`);
});

module.exports = router;