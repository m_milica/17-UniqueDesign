const jwt = require('jsonwebtoken');
const {jwt_secret} = require('../config/auth.config.js');

const verifyToken = (req, res, next) => {
    try {
        const authHeader = req.header("Authorization");
        if (!authHeader) {
          const error = new Error('Nedostaje zaglavlje za autentikaciju!');
          error.status = 403;
          throw error;
        }
    
        const token = authHeader.split(' ')[1];

        jwt.verify(token, jwt_secret, (err, payload)=>{  
          if (err) {
            const error = new Error('Token nije validan!');
            error.status = 401;
            throw error;
          }
    
          req.username = payload.username;    
          next();
        });
      } catch (err) {
        next(err);
      }    
};

module.exports = {
    verifyToken
};