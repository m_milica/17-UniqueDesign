export interface IJWTTokenData {
  name: string;
  surname: string;
  email: string;
  username: string;
  password: string;
   imgUrl: string;
}
