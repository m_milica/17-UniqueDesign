import { User } from './../../users/models/user.model';
import { Subscription } from 'rxjs';
import { AuthService } from './../../users/services/auth.service';
import { Component, OnInit, OnDestroy } from '@angular/core';

@Component({
  selector: 'app-nav-menu',
  templateUrl: './nav-menu.component.html',
  styleUrls: ['./nav-menu.component.css']
})
export class NavMenuComponent implements OnInit, OnDestroy{
  userSub: Subscription;
  user: User;

  constructor(private authService: AuthService) {
    this.userSub = authService.user.subscribe((user:User) => {
      this.user = user;
    });
    authService.sendUserDataIfExists();
  }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
    if(this.userSub){
      this.userSub.unsubscribe();
    }
  }
}
