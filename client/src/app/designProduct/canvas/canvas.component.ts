import { CdkDragEnd } from '@angular/cdk/drag-drop';
import { Component, EventEmitter, HostListener, Input, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { Image as Img } from '../models/image.model';
import { Text } from '../models/text.model';
import { DesignService } from '../services/design.service';

@Component({
  selector: 'app-canvas',
  templateUrl: './canvas.component.html',
  styleUrls: ['./canvas.component.css']
})
export class CanvasComponent implements OnInit {

  public clothingItems: any[];
  public selectedItemId: string;
  public selectedItem: any;
  public selectedSize: string;
  public selectedSide: string;
  
  public toolToAdd;
  public imgTools: Img[] = [];
  public textTools: Text[] = [];

  @Input() selectedTool: any;
  @Output() selectedToolChange = new EventEmitter<any>();
  
  _product;
  get product() {
    return this._product;
  }

  @Input() set product(value) {
    this._product = value;
    if (value) {
      this.selectedSize = value.size;
    
      this.selectedItemId = value.idClothes;

      if (this.selectedItemId) {
        this.sDesign.getClothingItemsById(this.selectedItemId).subscribe(res => {
          this.selectedItem = res;    
        })
      }

      if (value._id) {
        this.sDesign.getImgToolsByProductId(value._id).subscribe(imgTools => {
          this.imgTools = imgTools.map(imgTool => {
            return new Img (
              imgTool.imageContent,
              imgTool.opacity,
              imgTool.width,
              imgTool.height,
              imgTool.positionX,
              imgTool.positionY,
              imgTool.flag
            );
          });
        })

        this.sDesign.getTextToolsByProductId(value._id).subscribe(textTools => {
          this.textTools = textTools.map(textTool => {
            return new Text (
              textTool.textContent,
              textTool.size,
              textTool.color,
              textTool.positionX,
              textTool.positionY,
              textTool.flag
            );
          });
        })
      }

      this.selectedSide = 'f';
    }
  }

  constructor(
    private sDesign: DesignService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.sDesign.getClothingItems().subscribe(items => {
      this.clothingItems = items;
    });
  }

  onTypeChanged(itemId) {
    this.selectedItemId = itemId;
    if (this.selectedItemId) {
      this.selectedItem = this.clothingItems.find(item => { return item._id == this.selectedItemId })
    } else {
      this.selectedItem = null;
    }
    this.selectedSide = 'f';
  }

  onSizeChanged(size) {
    this.selectedSize = size;
  }

  setSelectedTool(tool, event?) {
    this.selectedTool = tool;
    this.selectedToolChange.emit(this.selectedTool);
    
    event?.stopPropagation();
  }

  addTool(event) {
    let tool;
    if (this.toolToAdd) {
      switch(this.toolToAdd) {
        case 'IMG':
          tool = new Img("", 10, 50, 50, event.layerX, event.layerY, this.selectedSide);
          this.imgTools.push(tool);
          break;
        case 'TEXT':
          tool = new Text("Placeholder", 14, "black", event.layerX, event.layerY, this.selectedSide);
          this.textTools.push(tool);
          break;
      }   
    }

    this.toolToAdd = null;
    this.setSelectedTool(tool);
  }

  get imgToolsCurrentSide () {
    return this.imgTools.filter(img => { return img.flag == this.selectedSide })
  }

  get textToolsCurrentSide () {
    return this.textTools.filter(text => { return text.flag == this.selectedSide })
  }

  changeSide(side) {
    this.selectedSide = side;

    this.toolToAdd = null;
    this.setSelectedTool(null);
  }

  dragEnd($event: CdkDragEnd) {
    this.selectedTool.positionX += $event.distance.x;
    this.selectedTool.positionY += $event.distance.y;
  }

  saveProduct() {
    const newProduct = {
      name: this.product.name,
      idClothes: this.selectedItemId,
      size: this.selectedSize,
      color: this.product.color,
      public: this.product.pub,
      imgUrl: '/assets/ofinger.jpg'
    }
    
    this.sDesign.postProduct(newProduct).subscribe(
      res => {
        const productId = res._id
        this.imgTools.forEach(img => {
          let newImgTool = {
            ... img,
            productId: productId
          }

          this.sDesign.postImgTool(newImgTool).toPromise();
        })

        this.textTools.forEach(text => {
          let newTextTool = {
            ... text,
            productId: productId
          }

          this.sDesign.postTextTool(newTextTool).toPromise();
        })

        this.router.navigateByUrl('/user');
      },
      err => {
        if(err.status == 401) {
          window.alert("You are not logged in!");
        } else {
          window.alert("Product name, clothing item and size are required!")
        }

      }
    );
  }

  get backgroundImg() {
    const url = this.selectedSide == 'b' ? this.selectedItem.backImage : this.selectedItem.frontImage;
    return  "url('" + url + "')";
  }

  @HostListener('document:keyup', ['$event'])
  handleDeleteKeyboardEvent(event: KeyboardEvent) {
    if (event.key === 'Delete') {
      this.imgTools = this.imgTools.filter(img => { return img !== this.selectedTool })
      this.textTools = this.textTools.filter(text => { return text !== this.selectedTool })
      this.setSelectedTool(null);
      this.toolToAdd = null;
    }
  }
}
