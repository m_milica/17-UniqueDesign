export class Product{
    public constructor(
        public _id?: string,
        public name: string="Untitled",
        public size?: string,
        public color: string="#ffffff",
        public pub: boolean=false,
        public idClothes?: string,
        public idUser?: string
    ){}
}