export class Image{
    public constructor(
        public imageContent: string,
        public opacity: number,
        public width: number,
        public height: number,
        public positionX: number,
        public positionY: number,
        public flag: string
    ){}
}