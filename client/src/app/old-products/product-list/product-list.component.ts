import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { Product } from '../models/product';
import { ProductService } from '../services/product.service';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css']
})
export class ProductListComponent implements OnInit, OnDestroy {

  public products: Product[] = [];
  private activeSubscriptions: Subscription[] = [];

  constructor(private productService: ProductService) { 
    const sub = this.productService.getProducts()
      .subscribe((products: Product[]) => {
        this.products = products;
      });
      this.activeSubscriptions.push(sub);
  }

  ngOnInit() {
  }

  ngOnDestroy() {
    this.activeSubscriptions.forEach(sub => sub.unsubscribe());
  }

}
