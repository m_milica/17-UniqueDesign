import { Component, OnDestroy, OnInit } from '@angular/core';
import { Product } from '../models/product';
import { ProductService } from '../services/product.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { StoreService } from '../../store/services/store.service';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit, OnDestroy {

  public product: Product;

  private paramMapSub: Subscription = null;

  constructor(private route: ActivatedRoute,
            private productService: ProductService,
            private storeService: StoreService,
            private router: Router) {
            this.paramMapSub = this.route.paramMap.subscribe(params => {
              const pId = params.get('productId');

              this.productService.getProductById(pId)
                .subscribe((product: Product) => {
                      this.product = product;
                });
            });
  }

  ngOnInit(): void {
  }

  ngOnDestroy() {
    if (this.paramMapSub != null) {
      this.paramMapSub.unsubscribe();
      this.paramMapSub = null;
    }
  }

  public addToCart() {
    this.storeService.addToCart(this.product);
    window.alert('Your product has been added to the cart!');
    this.router.navigateByUrl("product-list");
  }

}
