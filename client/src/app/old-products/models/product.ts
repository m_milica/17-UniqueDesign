export interface Product {
    _id: string,
    name: string;
    size: string;
    public: boolean;
    price: number;
    idClothes: string;
    idUser: string;
    imgUrl: string;
    
}