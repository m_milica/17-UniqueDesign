import { Product } from './../models/product';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { JwtService } from 'src/app/common/services/jwt.service';

@Injectable({
  providedIn: 'root'
})
export class ProductService {
  private products: Observable<Product[]>;
  private page = 1;
  private result;
  private readonly productsUrl = 'http://localhost:5001/products/';
  private readonly userProductsUrl = 'http://localhost:5001/products/user'

  constructor(private http: HttpClient,private  jwtService: JwtService) {
    this.refreshProducts();
  }

  private refreshProducts(): Observable<Product[]> {
    do {
      this.result  = this.http.get<Product[]>(`${this.productsUrl}?page=${this.page++}&limit=10`);
      this.products = this.result;
    } while (this.result.page < this.page)

    return this.products;
  }

  public getProducts(): Observable<Product[]> {
    return this.products;
  }

  public getProductById(id: string): Observable<Product> {
    return this.http.get<Product>(this.productsUrl + id);
  }

  public getProductsByUser(): Observable<Product[]>{
    const body = {};
    const headers: HttpHeaders = new HttpHeaders().append("Authorization", `Bearer ${this.jwtService.getToken()}`);
    return this.http.get<Product[]>(this.userProductsUrl,{headers});
  }

}
