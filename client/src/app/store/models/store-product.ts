import { Product } from '../../old-products/models/product';

export interface StoreProduct {
  _id: string;
  products: string[] | Product[];
  name: string;
  address: string;
  email: string;
}