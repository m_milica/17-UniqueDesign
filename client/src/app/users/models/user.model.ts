export class User {
    public constructor(
        public name: string,
        public surname: string,
        public email: string,
        public username: string,
        public password: string,
        public imgUrl: string = ''
    ) { }
}
