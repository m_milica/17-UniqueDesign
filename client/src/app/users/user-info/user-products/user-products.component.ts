import { Product } from './../../../old-products/models/product';
import { Observable, Subscription } from 'rxjs';
import { Component, Input, OnInit, OnDestroy } from '@angular/core';
import { ProductService } from 'src/app/old-products/services/product.service';

@Component({
  selector: 'app-user-products',
  templateUrl: './user-products.component.html',
  styleUrls: ['./user-products.component.css']
})
export class UserProductsComponent implements OnInit, OnDestroy {
  products: Product[];
  private sub: Subscription;

  constructor(private productService: ProductService) {
    this.sub = this.productService.getProductsByUser()
    .subscribe((products: Product[]) => {
      this.products = products;
    });
   }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
    if(this.sub) this.sub.unsubscribe();
  }
}
