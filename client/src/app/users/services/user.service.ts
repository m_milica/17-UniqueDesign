import { Observable } from 'rxjs';
import { AuthService } from './auth.service';
import { JwtService } from './../../common/services/jwt.service';
import { HttpClient, HttpClientModule, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { User } from '../models/user.model';
import { map, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private readonly url = {
    putUpadeUser: 'http://localhost:5001/users/update/',
    putUpdatePassword: 'http://localhost:5001/users/',
    putUpdateUserProfileImage: 'http://localhost:5001/users/updateProfileImage/'
  };

  constructor(private http: HttpClient, private jwt: JwtService, private auth: AuthService)
  { }

  public updateUser(username:string, name:string, surname: string, email:string): Observable<User>{
    const body = {username, name, surname, email};
    const headers: HttpHeaders = new HttpHeaders().append("Authorization", `Bearer ${this.jwt.getToken()}`);
    return this.http.put<{token: string}>(this.url.putUpadeUser, body,{headers}).pipe(
       tap((response: {token: string}) => this.jwt.setToken(response.token)),
       map((response: {token: string}) => this.auth.sendUserDataIfExists())
     );
  }

  public updateUserPassword(username: string, oldPassword: string, newPassword: string): Observable<User>{
    const body = {username, oldPassword, newPassword};
    const headers: HttpHeaders = new HttpHeaders().append("Authorization", `Bearer ${this.jwt.getToken()}`);
    return this.http.put<{token: string}>(this.url.putUpdatePassword, body,{headers}).pipe(
      tap((response: {token: string}) => this.jwt.setToken(response.token)),
      map((response: {token: string}) => this.auth.sendUserDataIfExists())
    );
  }

  public updateProfileImage(file:File):Observable<User>{
    const body: FormData = new FormData();
    body.append("file", file);
    const headers: HttpHeaders = new HttpHeaders().append("Authorization", `Bearer ${this.jwt.getToken()}`);
    return this.http.put<{token: string}>(this.url.putUpdateUserProfileImage, body,{headers}).pipe(
      tap((response: {token: string}) => this.jwt.setToken(response.token)),
      map((response: {token: string}) => this.auth.sendUserDataIfExists())
    );
  }

}
