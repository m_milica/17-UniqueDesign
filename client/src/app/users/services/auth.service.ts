import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, Subject } from 'rxjs';
import { User } from '../models/user.model';
import { JwtService } from 'src/app/common/services/jwt.service';
import { IJWTTokenData } from 'src/app/common/models/jwt-token-data';
import { tap } from 'rxjs/operators';
import { map } from 'rxjs/operators';
import { Login } from '../models/login.model';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private readonly userSubject: Subject<User> = new Subject<User>();
  public readonly user: Observable<User> = this.userSubject.asObservable();

  private readonly url = {
    registerUser: 'http://localhost:5001/users/register/',
    loginUser: 'http://localhost:5001/users/login/'
  };
  private isLoggedIn: boolean = false;

  constructor(private http: HttpClient, private jwtService: JwtService) { }

  public get isUserLoggedIn(): boolean {
    return this.isLoggedIn;
  }

  public sendUserDataIfExists(): User {
    const payload: IJWTTokenData = this.jwtService.getDataFromToken();
    const user: User = payload
      ? new User(payload.name, payload.surname, payload.email, payload.username, payload.password, payload.imgUrl)
      : null;
    this.userSubject.next(user);
    this.isLoggedIn = user !== null;
    return user;
  }

  public registerUser(name: string, surname: string, username: string, password: string, email: string): Observable<User> {
    const body = { name, surname, username, password, email };
    return this.http.post<{ token: string }>(this.url.registerUser, body).pipe(
      tap((response: { token: string }) => console.log(response.token)),
      map((response: { token: string }) => { return this.sendUserDataIfExists(); }));
  }

  public loginUser(username: string, password: string): Observable<User> {
    const body = { username, password };
    return this.http.post<{ token: string }>(this.url.loginUser, body).pipe(
      tap((response: { token: string }) => this.jwtService.setToken(response.token)),
      map((response: { token: string }) => { return this.sendUserDataIfExists(); }));
  }

  public logoutUser(): void {
    this.jwtService.removeToken();
    this.userSubject.next(null);
    this.isLoggedIn = false;
  }

}
